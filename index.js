const express = require( 'express' );
const mongojs = require( 'mongojs' );
const bodyParser = require( 'body-parser' );

const porta = 4444;
const app = express();
const db = mongojs( 'crawlerjus', [ 'paginas' ]);

const rotas = require( './rotas/' )({ db });

app.use( bodyParser.json() );
app.use( bodyParser.urlencoded({ extended: true }) );
app.use( '/html', express.static( __dirname + '/html' ));

app.get( '/processo/:tribunal/:numero', rotas.PROCESSO.obter );

app.listen( porta );

console.log( ` *** Escutando na porta ${porta}` );
console.log( ` *** Acesse http://localhost:${porta}/html/` );
