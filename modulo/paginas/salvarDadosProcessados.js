const { ObjectId } = require( 'mongojs' );

module.exports = ({ db, dados, dadosProcessados }) => {
	const { url, tribunal, processo } = dados;

	setTimeout(() => db.paginas.update(
		{
			url,
			tribunal,
			processo,
		},
		{$set: { dadosProcessados }}
	), 10 );
};
