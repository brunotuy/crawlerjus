module.exports = ({ db, dados }) => new Promise(( resolve, reject ) => {
	if ( !dados.url ) {
		reject( 'URL inválida' );
	}

	else {
		db.paginas.find( dados, ( error, result ) => {
			if ( error ) {
				reject( error );
			}

			resolve( result );
		});
	}
});
