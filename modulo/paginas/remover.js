const { ObjectId } = require( 'mongojs' );

module.exports = ({ db, dados }) => {
	const { url, tribunal, processo } = dados;

	setTimeout(() => db.paginas.remove({
		url,
		tribunal,
		processo,
	}), 10 );
};
