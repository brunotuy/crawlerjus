const verificar = require( './_verificar.js' );
const acessarSalvar = require( './_acessarSalvar.js' );

module.exports = async ({ db, dados }) => {
	const paginasSalvas = await verificar({ db, dados });
	
	if ( paginasSalvas && paginasSalvas.length > 0 ) {
		return paginasSalvas.shift();
	}

	return await acessarSalvar({ db, dados });
};
