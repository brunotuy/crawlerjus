const https = require( 'https' );
const axios = require( 'axios' );

module.exports = async ({ db, dados }) => {
	const { data } = await axios({
		json: true,
		method: 'GET',
		url: dados.url,
		httpsAgent: new https.Agent({ rejectUnauthorized: false })
	});

	dados.data = new Date();
	dados.html = data;

	db.paginas.save( dados );
	
	return dados;
};
