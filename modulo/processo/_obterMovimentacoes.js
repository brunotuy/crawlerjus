module.exports = ({ $ }) => {
	const list = [];
	const tbody = $( 'tbody#tabelaTodasMovimentacoes' );

	if ( tbody.length > 0 ) {
		const rows = $( 'tr', tbody );

		for ( let rowIdx = 0; rowIdx < rows.length; rowIdx++ ) {
			const tds = $( 'td', rows[rowIdx] );

			if ( tds.length > 2 ) {
				const date = $( tds[0] ).text().replace( /[\n|\t]/g, '' );
				const text = $( tds[2] ).text().replace( /[\t]/g, '' ).trim();
				const rowsText = text.split( '\n' ).filter( r => r.trim().length > 0 );

				list.push({ date, text: rowsText.join( '\n' ) });
			}
		}
	}

	return list;
};
