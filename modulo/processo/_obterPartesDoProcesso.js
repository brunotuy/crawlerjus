module.exports = ({ $ }) => {
	const partes = {};
	const tables = $( 'table#tableTodasPartes' );

	if ( tables.length > 0 ) {
		const rows = $( 'tr', tables[0] );

		for ( let key in rows ) {
			if ( !isNaN( key ) ) {
				const findTd = $( 'td', rows[key] );

				if ( findTd.length === 2 ) {
					const quem = $( findTd[0] ).text().replace( /[:|\n|\t|\s]/g, '' );
					const dadosTd = $( findTd[1] ).html();
					const linhasTd = dadosTd.split( '<br>' ).map( i => $( '<span>'+i.trim()+'</span>' ).text().replace( /[\n|\t]/g, '' ) );
					const obj = {nome: linhasTd.shift()};

					if ( !partes[quem] ) {
						partes[quem] = [];
					}

					for ( let linha = 0; linha < linhasTd.length; linha++ ) {
						const array = linhasTd[linha].split( ':' );
						const parte = array.shift();
						const nome = array.shift().trim();

						if ( !obj[parte] ) {
							obj[parte] = [];
						}

						obj[parte].push( nome );
					}

					partes[quem].push( obj );
				}
			}
		}
	}

	return partes;
};
