const paginas = require( '../paginas/' );
const processarDados = require( './_processarDados.js' );

module.exports = async ({ db, tribunal, processo }) => {
	let url;

	switch ( tribunal ) {
		case 'TJSP':
			url = 'https://esaj.tjsp.jus.br/cpopg/';
			break;

		case 'TJMS':
			url = 'https://www.tjms.jus.br/cpopg5/';
			break;

		default:
			throw 'Não sei processar dados do tribunal informado.';
	}

	const arrayProcesso = processo.split( '.' );

	if ( arrayProcesso.length !== 5 ) {
		throw 'Número de processo inválido.';
	}

	const processoComAno = `${arrayProcesso.shift()}.${arrayProcesso.shift()}`;
	const numeroUnificado = arrayProcesso.pop();
	const parametros = `search.do?conversationId=&dadosConsulta.localPesquisa.cdLocal=-1&cbPesquisa=NUMPROC&dadosConsulta.tipoNuProcesso=UNIFICADO&numeroDigitoAnoUnificado=${processoComAno}&foroNumeroUnificado=${numeroUnificado}&dadosConsulta.valorConsultaNuUnificado=${processo}&dadosConsulta.valorConsulta=&uuidCaptcha=`;
	const dadosPesquisa = { tribunal, processo, url: `${url}${parametros}` };
	const pagina = await paginas.obter({ db, dados: dadosPesquisa });

	return pagina.dadosProcessados
		? pagina.dadosProcessados
		: processarDados({ db, dadosPesquisa, html: pagina.html });
};
