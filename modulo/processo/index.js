const fs = require( 'fs' );
const funcoes = {};

fs
	.readdirSync(__dirname)
 	.filter(( file ) => file.indexOf( '.' ) !== 0 && file.indexOf( '_' ) !== 0 && file !== 'index.js' )
	.forEach(( file ) => {
		const nome = file.substring( 0, file.indexOf( '.' ) );
		const funcao = require( './'+file );

		funcoes[nome] = funcao;
	});

module.exports = funcoes;
