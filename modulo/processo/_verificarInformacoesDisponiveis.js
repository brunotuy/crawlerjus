module.exports = ({ $ }) => {
	const mensagemAlerta = $( 'td#mensagemRetorno > li' );

	return mensagemAlerta.length === 0
		? true
		: !$( mensagemAlerta ).text().trim() === 'Não existem informações disponíveis para os parâmetros informados.';
};