module.exports = ({ $ }) => {
	const dados = {};
	const tables = $( 'table.secaoFormBody' );

	if ( tables.length > 1 ) {
		const rows = $( 'tr', tables[1] );

		for ( let key in rows ) {
			if ( !isNaN( key ) ) {
				const findLabel = $( 'label', rows[key] ).text();
				const findSpanLabel = $( 'span.labelClass', rows[key] ).text();
				let label = false;
				let value = false;
				
				if ( findLabel && findLabel.length > 0 ) {
					const findValue = $( 'span', rows[key] );
					label = findLabel.replace( ':', '' ).trim();

					if ( findValue && findValue.length > 0 ) {
						value = $( findValue[0] ).text().trim();

						if ( label === 'Valor da ação' ) {
							value = value.replace( 'R$', '' ).trim();
						}
					}
				}
				else if ( findSpanLabel && findSpanLabel.length > 0 ) {
					label = findSpanLabel.replace( ':', '' ).trim();
					value = $( rows[key] ).text().replace( findSpanLabel, '' ).trim();
				}

				if ( label && value ) {
					dados[label] = value;
				}
			}
		}
	}

	return dados;
};
