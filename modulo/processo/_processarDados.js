const cheerio = require( 'cheerio' );
const paginas = require( '../paginas/' );
const obterMovimentacoes = require( './_obterMovimentacoes.js' );
const obterDadosDoProcesso = require( './_obterDadosDoProcesso.js' );
const obterPartesDoProcesso = require( './_obterPartesDoProcesso.js' );
const verificarInformacoesDisponiveis = require( './_verificarInformacoesDisponiveis.js' );

module.exports = ({ db, dadosPesquisa, html }) => {
	const $ = cheerio.load( html );
	const informacoesDisponiveis = verificarInformacoesDisponiveis({ $ });

	if ( !informacoesDisponiveis ) {
		paginas.remover({
			db,
			dados: dadosPesquisa,
		});

		throw 'Processo não encontrado.';
	}

	const dados = obterDadosDoProcesso({ $ });
	const partes = obterPartesDoProcesso({ $ });
	const movimentacoes = obterMovimentacoes({ $ });
	const dadosProcessados = { dados, partes, movimentacoes };

	paginas.salvarDadosProcessados({
		db,
		dadosProcessados,
		dados: dadosPesquisa,
	});					

	return dadosProcessados;
};
