const fs = require( 'fs' );

module.exports = ( params ) => {
	const objRotas = {};

	fs
		.readdirSync(__dirname)
	 	.filter(( file ) => file.indexOf( '.' ) !== 0 && file !== 'index.js' )
		.forEach(( file ) => {
			const nome = file.substring( 0, file.indexOf( '.' ) ).toUpperCase();
			const rotas = require( './'+file )( params );

			objRotas[nome] = rotas;
		});

	return objRotas;
};
