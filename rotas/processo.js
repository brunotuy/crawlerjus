module.exports = ({ db }) => {
	const processo = require( '../modulo/processo' );

	const obter = async ( req, res ) => {
		try {
			const { numero, tribunal } = req.params;

			const resposta = await processo.obter({
				db,
				processo: numero,
				tribunal: tribunal.toUpperCase(),
			});

			res.status( 200 ).json( resposta );
		}
		catch ( e ) {
			console.log( 'error', e );
			res.status( 500 ).json({ mensagem: e });
		}
	};

	return ({ obter });
};
